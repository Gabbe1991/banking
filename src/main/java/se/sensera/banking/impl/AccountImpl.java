package se.sensera.banking.impl;

import lombok.AllArgsConstructor;
import lombok.Data;
import se.sensera.banking.Account;
import se.sensera.banking.User;

import java.util.stream.Stream;
@Data
@AllArgsConstructor
public class AccountImpl implements Account {
    User owner;


    String id;
    String name;
    String personalIdentificationNumber;
    boolean active;
}



