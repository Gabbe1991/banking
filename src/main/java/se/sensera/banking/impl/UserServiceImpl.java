package se.sensera.banking.impl;


import jdk.jshell.spi.ExecutionControl;
import se.sensera.banking.User;
import se.sensera.banking.UserService;
import se.sensera.banking.UsersRepository;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserServiceImpl implements UserService {
    private UsersRepository usersRepository;

    public UserServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public User createUser(String name, String personalIdentificationNumber) throws UseException {
        List<User> Userlist = usersRepository.all().collect(Collectors.toList());
        for(User U: Userlist){
            if(Objects.equals(U.getPersonalIdentificationNumber(), personalIdentificationNumber)){
                throw new UseException(Activity.CREATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
            }
        }


        UserImpl user = new UserImpl(UUID.randomUUID().toString(),name, personalIdentificationNumber, true);
        usersRepository.save(user);
        return user;
    }

    @Override
    public User changeUser(String userId, Consumer<ChangeUser> changeUser) throws UseException {
        if(!getUser(userId).isPresent()){
            throw new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND);
        }
        User user = getUser(userId).get();

        changeUser.accept(new ChangeUser() {
            @Override
            public void setName(String name) {
                user.setName(name);
                usersRepository.save(user);


            }

            @Override
            public void setPersonalIdentificationNumber(String personalIdentificationNumber) throws UseException {
                List<User> Userlist = usersRepository.all().collect(Collectors.toList());
                for(User U: Userlist){
                    if(Objects.equals(U.getPersonalIdentificationNumber(), personalIdentificationNumber)){
                        throw new UseException(Activity.UPDATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
                    }
                }
            user.setPersonalIdentificationNumber(personalIdentificationNumber);
                usersRepository.save(user);

            }
        });

        return user;


    }

    @Override
    public User inactivateUser(String userId) throws UseException {
        if(!getUser(userId).isPresent()) {
            throw new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND);
        }
        User user = getUser(userId).get();
        user.setActive(false);
        usersRepository.save(user);
        return user;
    }

    @Override
    public Optional<User> getUser(String userId) {
        return usersRepository.getEntityById(userId);
    }

    @Override
    public Stream<User> find(String searchString, Integer pageNumber, Integer pageSize, SortOrder sortOrder) {
        if(searchString == "" && pageNumber == null && pageSize == null && sortOrder == UserService.SortOrder.None){

            return usersRepository.all().filter(user -> user.isActive());
        }
        if(searchString == "" && pageNumber == null && pageSize == null && sortOrder == UserService.SortOrder.PersonalId){
            return usersRepository.all().sorted(Comparator.comparing(User::getPersonalIdentificationNumber));

        }
        if(searchString == "" && pageNumber == null && pageSize == null && sortOrder == SortOrder.Name){
            return usersRepository.all().sorted(Comparator.comparing(User::getName));

        }

        if(!Objects.equals(searchString, "")){

            return usersRepository.all().filter(user -> user.getName().toLowerCase(Locale.ROOT).contains(searchString)&&
                     user.isActive());


        }

    return usersRepository.all().filter(user -> user.isActive());
    }
}

